import { SagaIterator } from '@redux-saga/core'
import _, { Dictionary } from 'lodash'
import moment from 'moment-timezone'
import { call, cancel, cancelled, delay, fork, put, select, take, takeEvery, takeLatest } from 'redux-saga/effects'
import { v4 as uuidv4 } from 'uuid'
import { createAction, NET_ACTION } from './actions'
import { getLastActivityDate } from './dateService'
import { doFetch, setApiRoot } from './fetchService'
import {
	EndpointConfig,
	EndpointMapping,
	EndpointMappings,
	ErrorHandler,
	FetchAction,
	FetchConfig,
	FetchError,
	FetchResult,
	LoggerFunction,
	Model,
	ModelCollection,
	OAuthTokenResponse,
	TokenAccessFunction
} from './types'

/** The total number of tries for `fetchData`, including the initial request. */
export const TRY_LIMIT = 5

//#region Helpers

export interface PrepareFetchResult {
	endpointMapping: EndpointMapping
	fetchConfig: FetchConfig
	endpointConfig: EndpointConfig
	modelName: string
	isCollectionItemFetch: boolean
	isCollectionItemCreate: boolean
	isUrlValid: boolean
}

export interface GenericModel extends Model, Dictionary<any> {}

export const getState = (state?: any) => state

export const matchesTerminationAction = (incomingAction: any, fetchAction: any) => {
	return (
		incomingAction.type === NET_ACTION.PERIODIC_TERMINATION_REQUESTED &&
		incomingAction.taskId === fetchAction.taskId
	)
}

export const takeMatchesTerminationAction = (action: any) => (incomingAction: any) =>
	matchesTerminationAction(incomingAction, action)

/* istanbul ignore next */
/* eslint-disable-next-line require-yield */
export const defaultTokenAccessFunction: TokenAccessFunction = function* () {
	return null
}

/* istanbul ignore next */
export const defaultErrorHandler: ErrorHandler = () => {
	return
}

export const getChildCollectionKeys = (endpointMapping: EndpointMapping) =>
	Object.keys(endpointMapping).reduce((out: string[], key) => {
		const value = endpointMapping[key]
		if (!value || !Object.prototype.hasOwnProperty.call(value, '_config')) {
			return out
		}
		const em = value as EndpointMapping
		if (!!em._config && !!em._config.isCollection) {
			out.push(key)
		}
		return out
	}, [])

export const convertCollections = (
	endpointMapping: EndpointMapping,
	prepareFetchResult: PrepareFetchResult,
	data: any
) => {
	const { endpointConfig, fetchConfig, isCollectionItemCreate, isCollectionItemFetch } = prepareFetchResult
	const childCollectionKeys = getChildCollectionKeys(endpointMapping)
	const hasChildCollections = childCollectionKeys.length > 0
	// no collections, return as-is
	if (!endpointConfig.isCollection && !hasChildCollections) {
		return data
	}
	// delete returns no data
	if (fetchConfig.method === 'DELETE') {
		return {}
	}
	// data is a collection item, convert any child collections in item response
	if (isCollectionItemFetch || isCollectionItemCreate) {
		return convertDataCollectionItem(data, childCollectionKeys, endpointMapping)
	}
	// data is a collection, convert collection response, and any child collections in each item
	return endpointConfig.isCollection
		? convertDataToCollection(data, childCollectionKeys, endpointMapping)
		: convertDataCollectionItem(data, childCollectionKeys, endpointMapping)
}

export const convertDataCollectionItem = (data: any, collectionKeys: string[], endpointMapping: EndpointMapping) => {
	if (_.isNil(data)) {
		return data
	}
	return Object.keys(data).reduce((out: GenericModel, key) => {
		let value = data[key]
		if (collectionKeys.includes(key)) {
			const childEndpointMapping = endpointMapping[key] as EndpointMapping
			const childCollectionKeys = getChildCollectionKeys(childEndpointMapping)
			value = convertDataToCollection(value, childCollectionKeys, childEndpointMapping)
		}
		out[key] = value
		return out
	}, {})
}

export const convertDataToCollection = (data: any, collectionKeys: string[], endpointMapping: EndpointMapping) => {
	if (_.isNil(data)) {
		return data
	}
	return Object.keys(data).reduce((out: ModelCollection<any>, key) => {
		const value = convertDataCollectionItem(data[key], collectionKeys, endpointMapping)
		/* istanbul ignore else */
		if (value && value.id) {
			out[value.id] = value
		}
		return out
	}, {})
}

/**
 * A default logger function that logs to the console. Used if no other logger is provided
 *
 * @param message The message to log
 */
const defaultLogger: LoggerFunction = (message?: any) => {
	console.debug(message)
}

/**
 * Using the `lastActivityDate` set in local storage by "studiokit-caliper-js",
 * determine if the user has been idle, e.g. has had no activity in the last 15 minutes.
 */
function isUserIdle() {
	const lastActivityDateString = getLastActivityDate()
	if (!lastActivityDateString) {
		return false
	}

	const lastActivityDateUtc = moment.utc(lastActivityDateString)
	const nowUtc = moment.utc()
	const idleMinutesThreshold = 1000 * 60 * 15 // 15 minutes

	return nowUtc.diff(lastActivityDateUtc) >= idleMinutesThreshold
}

//#endregion Helpers

//#region Local Variables

let logger: LoggerFunction
let endpointMappings: EndpointMappings
let tokenAccessFunction: TokenAccessFunction
let errorHandler: ErrorHandler

//#endregion Local Variables

/** Set the shared EndpointMappings variable, exposed for testability */
export function setEndpointMappings(endpointMappingsParam: EndpointMappings) {
	endpointMappings = endpointMappingsParam
}

/**
 * Prepare fetchConfig to pass to fetchService. Also set up state
 * to handle response correctly.
 *
 * @param action The action dispatched by the client
 * @param endpointMappingsParam The EndpointMappings object, passed in for testability
 */
export function prepareFetch(
	action: FetchAction,
	endpointMappingsParam: EndpointMappings = endpointMappings
): PrepareFetchResult {
	// Get fetch parameters from global fetch dictionary using the modelName passed in to locate them
	// Combine parameters from global dictionary with any passed in - locals override dictionary
	const endpointMapping: EndpointMapping = _.get(endpointMappingsParam, action.modelName)
	if (!endpointMapping) {
		throw new Error(`Cannot find '${action.modelName}' in 'endpointMappingsParam'`)
	}

	const endpointConfig = _.merge({}, endpointMapping._config)
	const fetchConfig = _.merge({}, endpointConfig.fetch, {
		headers: _.merge({}, action.headers),
		queryParams: _.merge({}, action.queryParams)
	})

	// set "method" if defined
	if (action.method) {
		fetchConfig.method = action.method
	}

	// set or merge "body"
	// If the body is a string, we are assuming it's an application/x-www-form-urlencoded
	if (!!action.body && (typeof action.body === 'string' || action.body instanceof FormData)) {
		fetchConfig.body = action.body
		fetchConfig.contentType = 'application/x-www-form-urlencoded'
	} else if (!!fetchConfig.body || !!action.body) {
		const isBodyArray = (fetchConfig.body && _.isArray(fetchConfig.body)) || (action.body && _.isArray(action.body))
		fetchConfig.body = isBodyArray
			? _.union([], fetchConfig.body, action.body)
			: _.merge({}, fetchConfig.body, action.body)
	}

	// set "contentType" if defined, overriding the default application/x-www-form-urlencoded
	// that may have been set previously
	if (action.contentType) {
		fetchConfig.contentType = action.contentType
	}

	let modelName: string = action.modelName
	let isCollectionItemFetch = false
	let isCollectionItemCreate = false
	let isUrlValid = true
	// copy pathParams into two arrays, to manage them separately
	let pathParams = _.merge([], action.pathParams)
	const modelNameParams = _.merge([], action.pathParams)

	// find all the model levels from modelName
	const modelNameLevels = modelName.split('.')
	let lastModelLevel: EndpointMapping | EndpointConfig = endpointMappingsParam
	const modelLevels = modelNameLevels.map(levelName => {
		const modelLevel = _.get(lastModelLevel, levelName)
		lastModelLevel = modelLevel
		return modelLevel
	})

	// find the levels that are collections
	const collectionModelLevels = modelLevels.filter(level => level._config && level._config.isCollection)
	const isAnyLevelCollection = collectionModelLevels.length > 0

	// if any level is a collection, we need to concat their fetch paths and modelNames
	if (isAnyLevelCollection) {
		if (modelNameLevels.length > 1) {
			modelLevels.forEach((modelLevel, index) => {
				const levelName: string = modelNameLevels[index]
				const currentModelConfig = _.merge({}, modelLevel._config)
				const currentFetchConfig = _.merge({}, currentModelConfig.fetch)
				const currentPath = !_.isUndefined(currentFetchConfig.path)
					? currentFetchConfig.path
					: index === 0
					? `/api/${levelName}`
					: levelName

				// first level, just use its values
				if (index === 0) {
					fetchConfig.path = currentPath
					modelName = levelName
					return
				}

				// if previous level isCollection, we need to use "{:id}" hooks when appending new level
				// otherwise, just append using the divider
				const prevModelConfig = _.merge({}, modelLevels[index - 1]._config)
				const divider = !!fetchConfig.path && fetchConfig.path.length > 0 && currentPath.length > 0 ? '/' : ''
				if (prevModelConfig.isCollection) {
					fetchConfig.path = `${fetchConfig.path}${divider}{:id}/${currentPath}`
					modelName = `${modelName}.{:id}.${levelName}`
				} else {
					fetchConfig.path = `${fetchConfig.path}${divider}${currentPath}`
					modelName = `${modelName}.${levelName}`
				}

				// an absolute path resets the fetch path, and ignores previous pathParams moving forward
				// it does not affect modelName params for redux
				if (currentPath.indexOf('/') === 0) {
					fetchConfig.path = currentPath
					const collectionLevelIndex = collectionModelLevels.indexOf(modelLevel)
					if (collectionLevelIndex > 0) {
						// since `collectionLevelIndex` is based on the full amount of params,
						// always target `modelNameParams`, not the "current" `pathParams`
						pathParams = modelNameParams.slice(collectionLevelIndex, modelNameParams.length)
					}
				}
			})
		}

		if (!fetchConfig.path) {
			fetchConfig.path = `/api/${modelName}`
		}

		// determine if we need to append an "{:id}" hook
		const pathLevels = (fetchConfig.path.match(/{:id}/g) || []).length
		// GET, PUT, PATCH, DELETE => append '/{:id}'
		isCollectionItemFetch = !!endpointConfig.isCollection && pathParams.length > pathLevels
		// POST
		isCollectionItemCreate = !!endpointConfig.isCollection && fetchConfig.method === 'POST'

		// insert pathParam hooks into path and modelName
		// track collection item requests by id (update, delete) or guid (create)
		if (isCollectionItemFetch && !isCollectionItemCreate) {
			fetchConfig.path = `${fetchConfig.path}/{:id}`
			modelName = `${modelName}.{:id}`
		} else if (isCollectionItemCreate) {
			modelName = `${modelName}.${action.guid || uuidv4()}`
		}
	}

	// substitute any params in path, e.g. /api/group/{:id}
	if (!!fetchConfig.path && /{:.+}/.test(fetchConfig.path)) {
		let index = 0
		fetchConfig.path = fetchConfig.path.replace(/{:(.+?)}/g, () => {
			const value = pathParams[index]
			if (value === undefined || value === null) {
				isUrlValid = false
			}
			index++
			return value
		})
	}

	// substitute any params in modelName, e.g. groups.{:id}
	if (/{:.+}/.test(modelName)) {
		let index = 0
		modelName = modelName.replace(/{:(.+?)}/g, () => {
			const value = modelNameParams[index]
			if (value === undefined || value === null) {
				isUrlValid = false
			}
			index++
			return value
		})
	}

	return {
		endpointMapping,
		fetchConfig,
		endpointConfig,
		modelName,
		isCollectionItemFetch,
		isCollectionItemCreate,
		isUrlValid
	}
}

/**
 * Construct a request based on the provided action, make a request with a configurable retry,
 * and handle errors, logging and dispatching all steps.
 *
 * @param action An action with the request configuration
 */
export function* fetchData(action: FetchAction): SagaIterator {
	// Validate
	if (!action || !action.modelName) {
		throw new Error("'modelName' config parameter is required for fetchData")
	}

	const prepareFetchResult = prepareFetch(action)
	const { fetchConfig, endpointConfig, endpointMapping, modelName, isCollectionItemCreate } = prepareFetchResult
	let { isUrlValid } = prepareFetchResult

	// TODO: Figure out how to move this into prepareFetch() without causing the
	// carefully constructed tower of yield()s in the tests from crashing down
	// substitute any path parameters from the redux store, e.g. '{{apiRoot}}/groups'
	if (!!fetchConfig.path && /{{.+}}/.test(fetchConfig.path)) {
		// have to get reference to the whole store here
		// since there is no yield in an arrow fn
		const store = yield select(getState)
		fetchConfig.path = fetchConfig.path.replace(/{{(.+?)}}/, (matches, backref) => {
			const value: any = _.get(store, backref)
			if (value === undefined || value === null) {
				isUrlValid = false
			}
			return value
		})
	}

	if (!isUrlValid) {
		yield put(
			createAction(action.noStore ? NET_ACTION.TRANSIENT_FETCH_FAILED : NET_ACTION.FETCH_FAILED, {
				modelName: action.modelName,
				guid: action.guid,
				errorData: 'Invalid URL'
			})
		)
		return
	}

	// Configure retry
	const tryLimit: number = action.noRetry ? 1 : TRY_LIMIT
	let tryCount = 0
	let didFail = false
	let lastFetchError: FetchError | undefined
	let lastError: Error | undefined
	// Run retry loop
	do {
		didFail = false
		tryCount++
		// Indicate fetch action has begun
		yield put(
			createAction(action.noStore ? NET_ACTION.TRANSIENT_FETCH_REQUESTED : NET_ACTION.FETCH_REQUESTED, {
				modelName,
				guid: action.guid
			})
		)
		let fetchResult: FetchResult | undefined = undefined
		try {
			const oauthToken: OAuthTokenResponse = yield call(tokenAccessFunction, action.modelName)
			if (oauthToken && oauthToken.access_token) {
				fetchConfig.headers = _.merge({}, fetchConfig.headers, {
					Authorization: `Bearer ${oauthToken.access_token}`
				})
			}
			fetchResult = yield call(doFetch, fetchConfig)
			if (fetchResult && fetchResult.ok) {
				const storeAction = action.noStore
					? NET_ACTION.TRANSIENT_FETCH_RESULT_RECEIVED
					: endpointConfig.isCollection && fetchConfig.method === 'DELETE'
					? NET_ACTION.KEY_REMOVAL_REQUESTED
					: NET_ACTION.FETCH_RESULT_RECEIVED

				const data = convertCollections(endpointMapping, prepareFetchResult, fetchResult.data)

				// attach guid to result
				if (action.guid && _.isPlainObject(data)) {
					data.guid = action.guid
				}

				// POST new collection item
				if (isCollectionItemCreate) {
					const modelNameLevels = modelName.split('.')
					// remove guid
					modelNameLevels.pop()
					// add by new result's id
					const resultAction = createAction(storeAction, {
						modelName: `${modelNameLevels.join('.')}.${data.id}`,
						guid: action.guid,
						data
					})
					yield put(resultAction)
					// remove temp item under guid key
					yield put(createAction(NET_ACTION.KEY_REMOVAL_REQUESTED, { modelName }))
				} else {
					const resultReceivedAction = createAction(storeAction, {
						modelName,
						guid: action.guid,
						data
					})
					yield put(resultReceivedAction)
				}
			} else {
				lastFetchError = {
					modelName,
					errorData: _.merge({}, !!fetchResult && !!fetchResult.data ? fetchResult.data : {})
				}
				throw new Error(JSON.stringify(lastFetchError))
			}
		} catch (error: any) {
			yield put(
				createAction(NET_ACTION.TRY_FETCH_FAILED, _.merge({ modelName, guid: action.guid }, lastFetchError))
			)

			// Don't do anything with 401 errors
			// And some errors don't have fetch results associated with them
			const errorData = lastFetchError ? lastFetchError.errorData : null
			if (errorData && errorData.code ? errorData.code !== 401 : true) {
				errorHandler(error, fetchConfig, fetchResult)
			}
			logger('fetchData fail')
			logger(error)

			// Don't retry after client error responses other than 408
			if (
				errorData &&
				errorData.code &&
				errorData.code >= 400 &&
				errorData.code < 500 &&
				errorData.code !== 408
			) {
				tryCount = tryLimit
			}

			didFail = true
			lastError = error
			if (tryCount < tryLimit) {
				yield delay(2 ** (tryCount - 1) * 100) // 100, 200, 400, 800
			}
		}
	} while (tryCount < tryLimit && didFail)

	// Handle retry failure
	if (tryCount === tryLimit && didFail) {
		yield put(
			createAction(
				action.noStore ? NET_ACTION.TRANSIENT_FETCH_FAILED : NET_ACTION.FETCH_FAILED,
				_.merge(
					{
						modelName,
						guid: action.guid
					},
					lastFetchError
				)
			)
		)
		logger('fetchData retry fail')
		logger(lastError)
	}
}

/**
 * Call the fetchData saga exactly one time (keeping in mind fetchData has retries by default)
 *
 * @param action An action with the request configuration
 */
export function* fetchOnce(action: FetchAction): SagaIterator {
	yield call(fetchData, action)
}

/**
 * The loop saga that makes the request every {action.period} milliseconds until
 * cancelled
 *
 * @param action An action with the request configuration
 */
export function* fetchDataLoop(action: FetchAction): SagaIterator {
	if (_.isNil(action.period)) {
		throw new Error('`action.period` is required')
	}
	try {
		let hasFetched = false
		do {
			// call the fetch if this is the first loop iteration,
			// or if the user is not idle
			if (!hasFetched || !isUserIdle()) {
				yield call(fetchData, action)
				hasFetched = true
			}
			yield delay(action.period)
		} while (true)
	} catch (error: any) {
		errorHandler(error)
		logger('fetchDataLoop fail')
		logger(error)
	} finally {
		if (yield cancelled()) {
			yield put(
				createAction(NET_ACTION.PERIODIC_TERMINATION_SUCCEEDED, {
					modelName: action.modelName
				})
			)
		}
	}
}

/**
 * Call the fetchData saga every {action.period} milliseconds. This saga requires the 'period' and 'taskId' properties
 * on the action parameter.
 *
 * @param action An action with the request configuration
 */
export function* fetchDataRecurring(action: FetchAction): SagaIterator {
	if (!action || !action.period) {
		throw new Error("'period' config parameter is required for fetchDataRecurring")
	}
	if (!action || !action.taskId) {
		throw new Error("'taskId' config parameter is required for fetchDataRecurring")
	}
	const bgSyncTask = yield fork(fetchDataLoop, action)
	yield take(takeMatchesTerminationAction(action))
	yield cancel(bgSyncTask)
}

/**
 * The main saga for fetching data. Must be initialized with an EndpointMappings object that can be fetched
 * and an API root to prepend to any partial URLs specified in the EndpointMappings object. A logger should normally be provided
 * as well.
 *
 * EndpointMappings object require a form as follows (with optional nested models):
 * ```
 * {
 * 	fryModel: {
 * 		path: '/api/Foo'
 * 	},
 * 	groupOfModels: {
 * 		leelaModel: {
 * 			path: '/api/Bar'
 * 		},
 * 		benderModel: {
 * 			path: '/api/Baz'
 * 		}
 * 	}
 * }
 * ```
 * EndpointMapping models are referenced in the actions.DATA_REQUESTED action by path, i.e.
 * `{ type: actions.DATA_REQUESTED, { modelName: 'fryModel' } }`
 * -- or --
 * `{ type: actions.DATA_REQUESTED, { modelName: 'groupOfModels.leelaModel' } }`
 *
 * @export
 * @param endpointMappingsParam A mapping of API endpoints available in the application
 * @param apiRootParam A url to which partial URLs are appended (i.e.) 'https://myapp.com'
 * @param tokenAccessFunctionParam function that returns
 * an optional OAuth token
 * @param errorHandlerParam A function that is called when any fetch exceptions occur
 * @param loggerParam A function that accepts a string and logs it real good
 */
export default function* fetchSaga(
	endpointMappingsParam: EndpointMappings,
	apiRootParam?: string,
	tokenAccessFunctionParam: TokenAccessFunction | undefined = defaultTokenAccessFunction,
	errorHandlerParam: ErrorHandler | undefined = defaultErrorHandler,
	loggerParam: LoggerFunction | undefined = defaultLogger
): SagaIterator {
	/* istanbul ignore if */
	if (!endpointMappingsParam) {
		throw new Error("'modelsParam' is required for fetchSaga")
	}
	setApiRoot(apiRootParam)
	logger = loggerParam
	logger(`logger set to ${logger.name}`)
	setEndpointMappings(endpointMappingsParam)
	errorHandler = errorHandlerParam
	tokenAccessFunction = tokenAccessFunctionParam

	yield takeEvery(NET_ACTION.DATA_REQUESTED, fetchOnce)
	yield takeEvery(NET_ACTION.PERIODIC_DATA_REQUESTED, fetchDataRecurring)
	yield takeLatest(NET_ACTION.DATA_REQUESTED_USE_LATEST, fetchOnce)
}
