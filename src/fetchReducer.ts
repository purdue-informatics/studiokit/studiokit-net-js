import _, { Dictionary } from 'lodash'
import _fp from 'lodash/fp'
import { NET_ACTION } from './actions'
import { FetchAction, Metadata, Model } from './types'

/**
 * Given the state and a path into that state object, return the prop that
 * is named "_metadata"
 *
 * @param state The redux state object
 * @param path An array of keys that represent the path to the entity in question
 */
export function getMetadata(state: Record<string, unknown>, path: string[]): Metadata {
	return _.merge({}, _.get(state, path.concat('_metadata')))
}

/**
 * Get whether or not an object is a "collection" (id key-value dictionary).
 * @param obj
 * @returns A boolean
 */
export function isCollection(obj: any) {
	return (
		!_.isArray(obj) &&
		_.isPlainObject(obj) &&
		Object.keys(obj).length > 0 &&
		Object.keys(obj).every(key => {
			const child = obj[key]
			return (
				_.isPlainObject(child) &&
				(key === '_metadata' ||
					(Object.prototype.hasOwnProperty.call(child, 'id') &&
						(child.id === parseInt(key, 10) || child.id === key)))
			)
		})
	)
}

/**
 * Get whether or not an object is a "null response".
 * @param obj
 * @returns A boolean
 */
export function isNullResponse(obj: any) {
	return (
		_.isPlainObject(obj) &&
		Object.keys(obj).length > 0 &&
		Object.keys(obj).every(key => {
			const child = obj[key]
			return key === '_metadata' || (key === 'response' && child === null)
		})
	)
}

/**
 * Merge the `current` and `incoming` objects recursively, returning a new combined object.
 *
 * For each key in `current`:
 * * remove if `current` is a "collection" and item key is not in `incoming`
 * * recurse if `incoming` is an object or array and has a value
 * * or preserve existing value
 *
 * For each key in `incoming` not in `current`:
 * * copy value to result
 *
 * @param current
 * @param incoming
 */
export function merge(current: Dictionary<any>, incoming?: Dictionary<any>) {
	// do not merge arrays
	if (_.isArray(current)) {
		return !_.isUndefined(incoming) ? incoming : current
	}
	// if incoming is `null` or a null response (`{'response': null}`), do not merge, just use incoming
	if (incoming === null || (!_.isUndefined(incoming) && isNullResponse(incoming))) {
		return incoming
	}
	const result: Dictionary<any> = Object.keys(current).reduce((out: Dictionary<any>, k) => {
		const c = current[k]
		const i = incoming && incoming[k]
		// non-relations
		if (!_.isArray(c) && !_.isPlainObject(c)) {
			// copy incoming value, if any, otherwise keep current
			out[k] = !_.isUndefined(i) ? i : c
			return out
		}
		// remove "collection" item not included in incoming
		if ((isCollection(current) || isCollection(incoming)) && !_.isUndefined(incoming) && _.isUndefined(i)) {
			return out
		}
		// merge relations
		out[k] = merge(c, i)
		return out
	}, {})
	if (!_.isNil(incoming) && _.isPlainObject(incoming)) {
		// copy incoming properties that are not on current
		Object.keys(incoming).forEach(k => {
			const c = current[k]
			const i = incoming[k]
			if (_.isUndefined(c)) {
				result[k] = i
			}
		})
	}
	return result
}

/**
 * Sets `_metadata` on any nested collections and collection items in the data
 * @param data
 * @param metadata
 */
export function updateMetadataForChildCollections(data: Dictionary<any>, metadata: Metadata) {
	Object.keys(data).forEach(key => {
		const value = data[key]
		if ((isCollection(data) && key !== '_metadata' && _.isPlainObject(value)) || isCollection(value)) {
			value._metadata = metadata
			updateMetadataForChildCollections(value, metadata)
		}
	})
}

/**
 * Reducer for fetching. Fetching state updated with every action. Data updated on result received.
 * Data and fetchedDate NOT deleted on failed request. All data at key removed on KEY_REMOVAL_REQUESTED.
 * All actions require a modelName key to function with this reducer.
 * Arrays are converted to objects that represent a dictionary with the numeric id of the object used
 * as the key and the entire object used as the value
 *
 * @export
 * @param state The state of the models. Initially empty
 * @param action The action upon which we dispatch
 * @returns The updated state
 */
export default function fetchReducer(state: Record<string, unknown> = {}, action: FetchAction) {
	if (!action.modelName) {
		return state
	}
	const path: string[] = action.modelName.split('.')
	// the object value at the specified path
	let valueAtPath: Model = _.merge({}, _.get(state, path))
	const metadata = getMetadata(state, path)
	let metadataUpdate: Metadata

	switch (action.type) {
		case NET_ACTION.FETCH_REQUESTED:
			// Retain the entity data, update the metadata to reflect
			// fetch in request state.
			metadataUpdate = {
				isFetching: true,
				hasError: false,
				lastFetchErrorData: undefined
			}
			valueAtPath._metadata = _.merge(metadata, metadataUpdate)
			if (action.guid) {
				valueAtPath.guid = action.guid
			}
			return _fp.setWith(Object, path, valueAtPath, state)

		case NET_ACTION.FETCH_RESULT_RECEIVED: {
			// Update the metadata to reflect fetch is complete.
			metadataUpdate = {
				isFetching: false,
				hasError: false,
				lastFetchErrorData: undefined,
				fetchedAt: new Date()
			}
			const incoming =
				!_.isPlainObject(action.data) && !_.isArray(action.data) ? { response: action.data } : action.data
			updateMetadataForChildCollections(incoming, metadataUpdate)
			valueAtPath = merge(valueAtPath, incoming)
			valueAtPath._metadata = _.merge({}, metadata, metadataUpdate)
			return _fp.setWith(Object, path, valueAtPath, state)
		}
		case NET_ACTION.FETCH_FAILED:
			// Retain the object, update the metadata to reflect the fact
			// that the request failed.
			metadataUpdate = {
				isFetching: false,
				hasError: true,
				lastFetchErrorData: action.errorData
			}
			valueAtPath._metadata = _.merge(metadata, metadataUpdate)
			if (action.guid) {
				valueAtPath.guid = action.guid
			}
			return _fp.setWith(Object, path, valueAtPath, state)

		case NET_ACTION.KEY_REMOVAL_REQUESTED:
			// Completely remove the object at the path from
			// the state.
			return _fp.unset(path, state)

		default:
			return state
	}
}
